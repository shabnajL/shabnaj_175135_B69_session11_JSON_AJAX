/**
 * Created by Web App Develop-PHP on 8/19/2017.
 */

var myJSONvariable = {
    "PropertyName1" : 123,
    "PropertyName2" : 12.3,
    "PropertyName3" : "ABC"
};
document.write("Harry Potter"+"<br>");
document.write(myJSONvariable.PropertyName3+"<br>");
document.write("<hr>");

var cat = {
    "name" : "TOM",
    "species" : "CAT",
    "favFood" : "Tuna"
};
document.write("I'm "+ cat.name +", I'm a "+ cat.species+". I like to eat " + cat.favFood+"<br>");
document.write("<hr>");
var animal = [
    "cat" , "dog" , "Bird"
];

animal["key1"] = "bbb";

document.write("<br>"+animal[1]+"<br>");
document.write("<br>"+animal["key1"]+"<br>");
document.write("<hr>");
var age = [];
age[0] = 123;
age["rahim"] = 30;
age["Karim"] = 45;
age["Abul"] = 44;

document.write(age["Karim"]+"<br>");
document.write("<hr>");
var animals = [

    {
        "name" : "TOM",
        "species" : "CAT",
        "Foods" : {
            "likes" : ["Tuna" , "Milk" ,"Rat"],
            "dislikes" : ["Spike's Food", "Cheese"]
        }
    },

    {
        "name" : "SPIKE",
        "species" : "DOG",
        "Foods" : {
            "likes" : ["Bone", "Meat"],
            "dislikes" : ["Milk", "Grass"]
        }
    },

    {
        "name" : "JERRY",
        "species" : "RAT",
        "Foods" : {
            "likes" : ["Cheese", "Ice-Cream","Sandwitch"],
            "dislikes" : ["Bone","Fish","Meat"]
        }
    }
];
for(i = 0; i<animals.length ;i++){

    var myHTML = "Name : " + animals[i].name + "<br>";
        myHTML += "species : " + animals[i].species + "<br>";
        myHTML += "Foods : <br> &nbsp;&nbsp; Likes : " + animals[i].Foods.likes + "<br>";
        myHTML += "&nbsp;&nbsp; Dislikes : " + animals[i].Foods.dislikes + "<br>";

    document.write(myHTML + "<hr>");
}